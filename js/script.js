// mini activity - recap ng last week session
let faveFood = "Siomai";
let sum = 150 + 9;
let product = 100 * 90;
let isActive = true;
let faveResto = ['Jollibee', 'Bonchon', 'Mang Inasal', 'Mcdo', 'Burger King'];

let faveArtist = {
	firstName: 'Taya',
	lastName: 'Gaukrodger',
	stageName: 'Taya Smith',
	birthday: 'May 10, 1989',
	age: 32,
	bestAlbum: 'People',
	bestSong: 'Another in the fire',
	isActive: true
};

console.log(faveFood);
console.log(sum);
console.log(product);
console.log(isActive);
console.log(faveResto);
console.log(faveArtist);

function divideNum(num1, num2) {
	return num1 / num2;
}
let quotient = divideNum(35, 5);
let word = "The result of the division is";
console.log(`${word} ${quotient}`);

//new session
// mathematical operators
let num1 = 5;
let num2 = 10;
let num3 = 4;
let num4 = 40;

//num1 = num1/*left operand*/ + num4/*right operand*/;
num1 += num4; //shorthand
console.log(num1);

num2 += num4;
console.log(num2);

num1 *= 2;
console.log(num1);

let string1 = 'Boston ';
let string2 = 'Celtics';
string1 += string2;
console.log(string1);

num1 = num1 - string1;
num1 -= string1;
console.log(num1);//NaN

//split
let string3 = "Hello everyone";
let myArray = string3.split("");
console.log(myArray);

//Mathematical operations - MDAS
let mdasResult = 1 + 2 - 3 * 4 / 5;
/*
	3*4 = 12
	12/5 = 2.4
	1+2 = 3
	3-2.4 = 0.6
*/
console.log(mdasResult);
//PEMDAS - parenthesis, exponents
let pemdasResult = 1 + (2-3) * (4/5);
/*
	4/5 = 0.8
	2-3 = -1
	-1*8 = -0.8
	1+ -0.8 = 0.1
*/
console.log(pemdasResult);

//increment and decrement
//two types of increment: pre-fix and post-fix
let z = 1;
//pre-fix incrementation
++z;
console.log(z);// result 2

//post-fix incrementation
z++;
console.log(z);// result 3
console.log(z++);// result 3
console.log(z);//result 4

//pre-fix vs post-fix
console.log(z++);
console.log(z);

console.log(++z);

let n = 1;
console.log(++n);// 1 + n = 2
console.log(n++); // n + 1 = 3 (pero hindi pa na return)
console.log(n);// 3


//pre-fix and post-fix decrementation
console.log(z);
console.log(--z);//immediate decrement

//post-fix
console.log(z--);
console.log(z);// need another round to display the result

//comparison operators - use to compare values, result is boolean
//equality or loose equality operator (==) - value lang
console.log(1 == 1); //true
console.log('1' == 1); //true
console.log('apple'  == 'Apple');//false - case sensitive

let isSame = 55 == 55;
console.log(isSame);//true

console.log(0 == false);//force coercion, false = 0
console.log(1 == true);// true, 1 is considered true
console.log(true == 'true');// false
console.log(true == '1');// true
console.log('0' == false);//true

//strict equality - checks value and data type
console.log(1 === 1); //true
console.log('1' === 1); //false
console.log('apple'  === 'Apple');//false- case sensitive
console.log('Juan' === 'Juan');//true

//inequality operators (!=)
//checks whtere the operands are not equal and/or have different value
//will do type coercion if the operands have different types:

console.log('1' != 1);//false
//both operands are converted to numbers
console.log('James' != 'John')//true
console.log(1 != true);//false
console.log(1 != 'true');//true

//string inequality operator (!==) -checks whether the two operand have different values and data type
console.log('5' !== 5);//true
console.log(5 !== 5);//false

let name1 = 'Juan';
let name2 = 'Maria';
let name3 = 'Pedro';
let name4 = 'Perla';

let number1 = 50;
let number2 = 60;
let numString1 = '50';
let numString2 = '60';

console.log(numString1 == number1);//true
console.log(numString1 === number1);//false
console.log(numString1 != number1);//false
console.log(name4 != name3);//true
console.log(name1 == 'juan');//false
console.log(name1 === 'Juan');//true

//relational comparison operators
// a comparison operator checks the relationship between the operands

let x = 500;
let y = 700;
let w = 8000;
let numString3 = '5500';

//greater than >
console.log( x > y);//false
console.log(w > y);//true
console.log(y > y);//false
console.log(numString3 > "Juan");//false

//less than <
console.log(w < y);//false
console.log(y < y);//false 700 < 699
console.log( x < 1000);
console.log(numString3 < 1000);//false
console.log(numString3 < 6000);//true
console.log(numString3 < "Juan");//true, erratic(logical error)
console.log(6000 < 'juan');//false

//logical operators
//And operator (&&)- both operands are true else false
/*
	T && T = T
	T && F = F
	F && T = F
	F && F = F
*/
let isAdmin = false;
let isRegistered = true;
let isLegalAge = true;

let authorization1 = isAdmin && isRegistered;
console.log(authorization1);//false

let authorization2 = isLegalAge && isRegistered;
console.log(authorization2);//true

let authorization3 = isAdmin && isLegalAge;
console.log(authorization3);//false

let requiredLevel = 95;
let requiredAge = 18;

let authorization4 = isRegistered && requiredLevel === 25;
console.log(authorization4);//false

let authorization5 = isRegistered && isLegalAge && requiredLevel === 95;
console.log(authorization5);

let userName1 = 'gamer2021';
let userName2 = 'shadowMaster';
let userAge1 = 15;
let userAge2 = 30;

let registeration1 = userName1.length > 9 && userAge1 >= requiredAge;
//.length - property of string which determines the number of characters in string
console.log(registeration1);//false

let registeration2 = userName2.length > 8 && userAge2 >= requiredAge;
console.log(registeration2);//true

let registeration3 = userName1.length > 8 && userAge2 >= requiredAge;
console.log(registeration3);//true

//OR operator (||) double pipe
//returns true if at least one of the operands are true
/*
	T || T = T
	T || F = T
	F || T = T
	F || F = F	
*/

let userLevel = 100;
let userLevel2 = 65;

let guildRequirement = isRegistered && userLevel >= requiredLevel && userAge1 >= requiredAge;
console.log(guildRequirement);

guildRequirement = isRegistered || userLevel >= requiredLevel || userAge1 >= requiredAge;
console.log(guildRequirement);

let guildRequirement2 = userLevel >= requiredLevel || userAge1 >= requiredAge;
console.log(guildRequirement2);

let guildAdmin = isAdmin || userLevel2 >= requiredLevel;
console.log(guildAdmin);//false

//not operator (!)
//turns a boolean into the opposite value T = F, F = T
let guildAdmin1 = !isAdmin || userLevel2 >= requiredLevel;
console.log(guildAdmin1);//true

let opposite1 = !isAdmin;
let opposite2 = !isLegalAge;

console.log(opposite1);//true - isAdmin orig value = false
console.log(opposite2);//false - isLegalAge orig value = true

//if-else
//if - will run a code block if the condition specified is true or results to true
const candy = 100;
if (candy >= 100){
	console.log('You got a cavity!')
};

/*
	if(true){
		block of code
	};
*/

let userName3 = "crusader_1993";
let userLevel3 = 25;
let userAge3 = 30;

if (userName3.length > 10){
	console.log("Welcome to the game online!")
};

//else statement will run if the condition given is falase or results to false
if (userName3 >= 10 && userLevel3 <= 25 && userAge3 >= requiredAge){
	console.log("Thank you for joining noobies guild");
} else{
	console.log("you too strong to be a noob");
}

//else if - if the previous or the original condition is false or resulted to false but another condition resulted to true.
if(userName3.length >= 10 && userLevel3 <= 25 && userAge3 >= requiredAge){
	console.log("Thank you for joining the noobies guild.");
} else if (userLevel3 > 25){
	console.log("You too strong to be noob");
} else if(userAge3 < requiredAge){
	console.log("You're too young to join the guild");
} else{
	console.log("end of the condition");
}

//if-else in function
function addNum(num1, num2){
	if(typeof num1 === "number" && typeof num2 === "number"){
		console.log("Run only if both arguments passed are number types");
		console.log(num1 + num2);
	} else{
		console.log("one or both of the arguments are not numbers")
	};
};
addNum(5,2);

//mini activity october 5
function login(username, password){
	if(typeof username === "string" && typeof password === "string"){
		console.log("both arguments are string");
		if(password.length >= 8){
			alert("password confirmed");
		}else{
			alert("credentials are too short");
		}
	}
	else if(username < 8){
		alert("username is too short");
	} else if(password < 8){
		alert("password too short");
	};
};
login("admin", "jollibee");